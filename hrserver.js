/* HuReS 
 * Humble Remote Switch 
 * Simple http tool switching relays using node.js on Raspberry Pi.
 * https://github.com/pliski/HuReS
 * 
 * Usage
 * 
 * Shell:
 * $ node hrserver.js
 * 
 * Browser
 * http://%ip%:%port%/index
 * 
 * or
 * 
 * http://%ip%:%port%/mobile
 * 
 * Ex.: http://192.168.1.8:8888/index 
 * 
 */

// REQUIRES
var config = require('config');
var fs = require('fs');
var url = require('url');
var http = require('http');
var map = require('through2-map');
var Lcd = require('lcd');
var async = require('async');
var ipUtil = require('ip');
var Relay = require('./Relay');
var arg = process.argv;


//CONFIG

//Displays and app Configs
var VERBOSE = config.get('App.verbose');
var VERSION = '0.4.2';
console.log("Humble Relay Switch - vers. " + VERSION);

var ip = config.get('App.ip');
if (ip === "auto") {
    ip = ipUtil.address().toString()
    console.log("Auto ip detect: " + ip)
}
var port = config.get('App.port');

var testButton = config.get('App.test_button');
if (testButton) {
    console.log("Test Button enabled: be careful!");
}

var oStatus = {}
oStatus.OFF = 0;
oStatus.ON = 1;
oStatus.ERR = 2;
oStatus.FEEDBACK_ERR = 3;
oStatus.FEEDBACK_OK = 4;

//Prepare GPIO pins
var aPin = config.get('Gpio.switches');
var aRelay = [];
var relCount = 0;
var switch_block = ""
var switch_line = fs.readFileSync("./frontend/switch_line.html", 'utf8');

//create relay instances and prepare dinamically the switches html block  
for (var k in aPin) {
    aRelay[relCount] = new Relay(aPin[k], k + "_" + relCount, relCount);
    switch_block += switch_line.replace(/%switch_id%/g, k + "_" + relCount).replace(/%switch_name%/g, k) + "\n";
    relCount++
}
//eventually add the test button
if (testButton) {
    switch_block += switch_line.replace(/%switch_id%/g, "test_button").replace(/%switch_name%/g, "Only for Test") + "\n";
    relCount++
}

//LCD
var flag_lcd = config.get('App.enable_lcd')
if (flag_lcd) {

    var aStatus = ['OFF', 'ON ', 'ERR']

    // lcd config
    var lcd = new Lcd({
        rs: config.get('Lcd.rs'),
        e: config.get('Lcd.e'),
        data: config.get('Lcd.data'),
        cols: config.get('Lcd.cols'),
        rows: config.get('Lcd.rows')
    });

    //Welcome display
    lcd.on('ready', function () {
        say("Lcd ready")
        lcd.clear()
        lcd.noAutoscroll();
        lcd.setCursor(0, 0);
        lcd.print("HuRes  port:" + port);
        lcd.once('printed', function () {
            lcd.setCursor(0, 1);
            lcd.print(' ' + ip);
        });
    });

    //Called at each http response 
    //Display the state of the switches
    function lcdStatus() {

        var lcd_out = '';
        for (var i = 0; i < relCount - 1; i++) {
            lcd_out += 'SW' + (aRelay[i].order + 1) + ' ' + aStatus[aRelay[i].status] + ' '
        }
        say("lcd status: " + lcd_out)

        lcd.clear();
        lcd.once('clear', function () {
            lcd.setCursor(0, 0);
            lcd.print(lcd_out.substring(0, 16));
            lcd.once('printed', function () {
                lcd.setCursor(0, 1);
                lcd.print(lcd_out.substring(16));
            });
        });
    }

}


// Map for filter outgoing files with config element
var mapObj = {"%port%": port, "%ip%": ip, "%version%": VERSION, "%switch_block%": switch_block, "\"%verbose%\"": config.get('App.verbose')};
var re = new RegExp(Object.keys(mapObj).join("|"), "gi");
function replaceAll(chunk) {
    return chunk.toString().replace(re, function (matched) {
        return mapObj[matched];
    });
}

//little function to manage the console output
function say(str, dir) {
    if (!VERBOSE) {
        return
    }

    if (dir) {
        console.dir(str);
    } else {
        console.log(str);
    }

}

//Smooth exit
function exit() {

    for (var i = 0; i < aRelay.length; i++) {
        aRelay[i].unexport();
    }

    if (flag_lcd) {
        lcd.close()
    }
    console.log("\nHuRes out.");
    process.exit();
}

//Common Async Response
function answer(msg, res) {

    myJson = JSON.stringify(msg);
    say(myJson, true);
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end(myJson);
    if (flag_lcd) {
        lcdStatus()
    }
}

// common async write
function writeSwitches(aGroupRelay, res) {

    var lastErr = null

    async.map(aGroupRelay, function (relayNum, callback) {

        var relay = aRelay[relayNum]

        say('Processing ' + relay.name);
        aRelay[relayNum].write(Number(relay.status), function (err) {

            output = {}

            //Non blocking errors
            if (err) {
                lastErr = "Error " + relay.name + " Check server console"
                console.log(lastErr);
                console.dir(err);
                if (typeof err.toString === 'function') {
                    console.log(err.toString());
                }
                relay.status = oStatus.ERR
            } else {
                say('End ' + relay.name);
            }

            output.name = relay.name
            output.status = relay.status

            callback(null, output);
        })

    }, function (err, aResult) {
        // if any of the file processing produced an error, err would equal that error
        if (err) {
            // One of the iterations produced an error.
            console.log('Got: ' + err);
            lastErr = err.toString();
        } else {
            say('All the relays have been processed successfully');
        }

        oFeedback = {}
        if (lastErr) {
            oFeedback.name = lastErr
            oFeedback.status = oStatus.FEEDBACK_ERR
        } else {
            oFeedback.name = "ok"
            oFeedback.status = oStatus.FEEDBACK_OK
        }
        //aResult contains the write results and a general feedback 
        //it will be send as response via JSON/Ajax       
        aResult.push(oFeedback)
        say(aResult, true)
        answer(aResult, res);

    });

}

//the SERVER
var server = http.createServer(function (req, res) {

// retriving the request params
    oMyUrl = url.parse(req.url, true);
    myPath = oMyUrl.pathname;
    say(myPath, true);
    switch (myPath) {

// controlling the switches 
        case '/switch':
        case '/switch_group':

            // aGroupRelay will contain the reference at the modified relay objects
            var aGroupRelay = []
            myquery = oMyUrl.query;
            say(myquery, true);

            //loop through the request param
            for (var key in myquery) {

                //filter out native properties
                if (myquery.hasOwnProperty(key)) {

                    //parameters convertion
                    //the actual id of the relay, which allow the coupling betwen frontend and server,
                    //it is dinamically assign at the object creation and store in the last part of the id of the html element
                    //here is sliced from the element' id
                    var relayNum = Number(key.slice(key.lastIndexOf("_") + 1));
                    var signal = Number(myquery[key]);

                    //TODO: test and improve the input filter
                    if (aRelay.indexOf(relayNum) && (signal === 1 || signal === 0)) {
                        aRelay[relayNum].status = signal
                        aGroupRelay.push(relayNum)
                    } else {
                        //Bad Arguments: normal flow is stopped and a message is sent to the frontend
                        var msg = {}
                        msg['feedback'] = "Not Found";
                        answer(msg, res);
                        say(key + " Not Found")
                        break
                    }

                }

            }
            writeSwitches(aGroupRelay, res)
            aGroupRelay = null
            break;

// retrieving the status of the pins
        case '/status':

            // lastErr will eventually contain only the last error that will be sent to the frontend
            var lastErr = null

            async.map(aRelay, function (relay, callback) {

                //onoff async read
                //TODO: more testing it's required to assure the asynchrony of the calls 
                say('Processing ' + relay.name);
                relay.read(function (err, value) {
                    output = {}

                    if (err) {
                        lastErr = "Error " + relay.name + " Check server console"
                        console.log(lastErr);
                        console.dir(err);
                        if (typeof err.toString === 'function') {
                            console.log(err.toString());
                        }
                        relay.status = oStatus.ERR

                    } else {
                        say('End ' + relay.name);
                        relay.status = Number(value)
                    }

                    output.name = relay.name
                    output.status = relay.status

                    callback(null, output);
                })

            }, function (err, aResult) {
                // landing point of all the calls
                // if any of the file processing produced an error, err would equal that error
                if (err) {
                    // One of the iterations produced an error.
                    console.log('Got: ' + err);
                    lastErr = err.toString();
                } else {
                    say('All the relays have been processed successfully');
                }

                oFeedback = {}
                if (lastErr) {
                    oFeedback.name = lastErr
                    oFeedback.status = oStatus.FEEDBACK_ERR
                } else {
                    oFeedback.name = "ok"
                    oFeedback.status = oStatus.FEEDBACK_OK
                }
                aResult.push(oFeedback)
                say(aResult, true)
                answer(aResult, res);

            });
            break

// reset the pin 
        case '/reset':
            var aGroupRelay = []
            for (var i = 0; i < aRelay.length; i++) {
                aRelay[i].status = oStatus.OFF
                aGroupRelay.push(i);
            }
            writeSwitches(aGroupRelay, res)
            break

// providing the index page
            /*
             * here and below I'm adapting the resources 
             * to the config parameters by replacing some keys.
             * 
             */
        case '/index':
            src = fs.createReadStream('./frontend/index.html');
            src.pipe(map(replaceAll)).pipe(res);
            break

// providing the frontend js functions
        case '/js':
            say("js");
            src = fs.createReadStream('./frontend/frontend.js');
            src.pipe(map(replaceAll)).pipe(res);
            break

// mobile page (using jQuery Mobile)
        case '/mobile':
            say("mobile");
            src = fs.createReadStream('./frontend/mobile.html');
            src.pipe(map(replaceAll)).pipe(res);
            break;


// add an image to the homecreen
        case '/favicon.ico':
        case '/apple-touch-icon.png':
            say("mobile");
            src = fs.createReadStream('./img/apple-touch-icon.png');
            src.pipe(res);
            break
            
        case '/led196.png':
            say("mobile");
            src = fs.createReadStream('./img/led196.png');
            src.pipe(res);
            break
            
// led img
        case '/img':
            myquery = oMyUrl.query;
            say(myquery.color);
            var src = null
            switch (myquery.color) {
                case 'red':
                    src = fs.createReadStream('./img/led_red.png');
                    break;
                case 'green':
                    src = fs.createReadStream('./img/led_green.png');
                    break;
                case 'orange':
                    src = fs.createReadStream('./img/led_orange.png');
                    break;
                default:
                    src = fs.createReadStream('./img/led_grey.png');
                    break;
            }
            src.pipe(res);
            break

//verify that jQuery is loading (for Debug)
        case '/mobiletest':
            say("mobiletest");
            src = fs.createReadStream('./frontend/mobiletest.html');
            src.pipe(map(replaceAll)).pipe(res);
            break;
// not found
        default:
            res.writeHead(404)
            res.end();
    }

}
);
// MAIN
server.listen(port)

console.log("Listening on " + ip + " port: " + port);
console.log("Try http://" + ip + ":" + port + "/index");

//Smooth exit. Releasing resources.
process.on('SIGINT', exit);