# HuReS
Humble Remote Switch <br> 
Simple http tool switching relays using node.js on Raspberry Pi.<br> 
Now support an optional lcd screen and a mobile web page.<br> 
<br> 

<h2>Usage</h2>
Install with npm<br>

    $ npm install hures
Connect the relay board to the RPI via your favorites pin. (see [Node.js Embedded Development on the Raspberry Pi](https://learn.adafruit.com/node-embedded-development/why-node-dot-js)) (gpio-admin may be required)<br>

Modify config/default.json or create your own config file (see [config](https://github.com/lorenwest/node-config))<br> 

    $ nano ./config/default.json
<br>
Fire!<br>

    $ node hrserver.js

Connect via your pc browser to http://"server_ip":"port"/index<br>
or via your mobile browser to http://"server_ip":"port"/mobile<br>
Ex.:
    http://192.168.1.8:8884/index

Well, that's all.<br><br>

If you want to add an lcd, you have to modify this line in the config/default.json file<br>

    "enable_lcd": true,
    
and check this Adafruit tutorial: [Wiring the LCD](https://learn.adafruit.com/drive-a-16x2-lcd-directly-with-a-raspberry-pi/wiring).<br>

Give it a try and let me know!

<h2>Tested on:</h2>
Raspberry pi B+<br>
Relay Board Keyes Funduino Ver.4R1B (4 relays)<br>
Standard lcd 16x2<br>

<h2>References</h2>
_ [Node.js Embedded Development on the Raspberry Pi](https://learn.adafruit.com/node-embedded-development/why-node-dot-js),<br> 
_ [Learnyounode](https://github.com/rvagg/learnyounode).

<h2>Based on:</h2>
node.js<br>
[onoff](https://github.com/fivdi/onoff)<br>
[config](https://github.com/lorenwest/node-config)<br>
[through2-map](https://github.com/brycebaril/through2-map)<br>
[async](https://github.com/caolan/async)<br>
[lcd](https://github.com/fivdi/lcd)<br>
[ip](https://github.com/indutny/node-ip)<br>
jQuery<br>
jQuery UI<br>
jQuery Mobile<br>
