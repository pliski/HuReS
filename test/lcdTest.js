'use strict';

// This example demonstrates how to call print twice in succession to display
// two strings at different positions on the LCD.
var Lcd = require('lcd'),
        lcd = new Lcd({rs: 26, e: 19, data: [13, 6, 5, 12], cols: 16, rows: 2});// Pi

lcd.on('error', function (err) {
    console.dir(err)
});

lcd.on('printed', function () {
    console.log('done printing')
});

var port = '4567890'
var test = '1234567890123' + port;

lcd.on('ready', function () {
    lcd.setCursor(0, 0);                                    // col 0, row 0
    lcd.print("1234567890123" + port);  // print time
    lcd.once('printed', function () {
        lcd.setCursor(0, 1);                                  // col 0, row 1
        lcd.print(test); // print date
        lcd.once('printed', function () {
            lcd.close();
        });
    });
});
