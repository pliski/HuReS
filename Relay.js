/* HuReS 
 * Humble Remote Switch 
 * Simple http tool switching relays using node.js on Raspberry Pi.
 * https://github.com/pliski/HuReS
 * 
 * Relay class
 * 
 */
var onoff = require('onoff').Gpio;

function Relay(gpio, name, order) {
    onoff.call(this, gpio, "low");
    this.status = 0
    this.name = name
    this.order = order
    console.log(this.name + " created. status: " + this.status)
}

Relay.prototype = Object.create(onoff.prototype);

Relay.prototype.getOrder = function () {
    return this.order;
}
Relay.prototype.getName = function () {
    return this.name;
}
Relay.prototype.getStatus = function () {
    return this.status;
}

module.exports = Relay;






