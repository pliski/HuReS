/* HuReS 
 * Humble Remote Switch 
 * Simple http tool switching relays using node.js on Raspberry Pi.
 * https://github.com/pliski/HuReS
 * 
 * frontend.js
 * common js file for index.html and mobile.html
 * parsed by the server substitute %% keys with config values
 * 
 */

$(document).ready(function () {

    var verbose = "%verbose%";

    //little function to manage the console output
    function say(str, dir) {
        if (verbose) {
            if (dir) {
                console.dir(str);
            } else {
                console.log(str);
            }
        }
    }

    var oGroup = {}

    $('#format input[type=checkbox]').change(function () {

        $("#led").attr("src", imgQuery + "orange");

        say(this.id);
        switch_id = this.id;
        if (switch_id === "test_button") {
            run_some_tests()
            return null
        }

        switch_val = 0;

        if ($(this).is(':checked')) {
            switch_val = 1;
        }

        oGroup[switch_id] = switch_val;

        if (!$("#group_button").is(':checked')) {

            ajaxSwitch("switch", oGroup)
        }
    });


    $('#group_button').click(function (event) {

        if (!$(this).is(':checked')) {
            led("grey")
            ajaxSwitch("switch_group", oGroup)
            $("#footer").empty();
        } else {
            $("#footer").empty().html("Relaese the Group button to activate the switches")
            led("orange")
        }
    })

    function resetGroup() {
        if ($('#group_button').is(':checked')) {
            $("#group_button").prop('checked', false);
//            led("grey")
            oGroup = {}
            buttonrefresh();
        }
    }

    $('#reset_button').click(function (event) {
        event.preventDefault();
        led("orange")
        resetGroup()
        ajaxSwitch("reset", null)
    });



    $('#status_button').click(function (event) {
        event.preventDefault();    
        led("orange")    
        resetGroup()
        ajaxSwitch("status", null)
    });

    var imgQuery = 'http://%ip%:%port%/img?color=';

    function ajaxSwitch(action, datasend) {

        $.getJSON("http://%ip%:%port%/" + action, datasend, function (ret) {

            $("#footer").empty();

            $.each(ret, function (idx, oRelay) {
                say(oRelay)

                key = oRelay.name
                sw_status = Number(oRelay.status)

                if (verbose) {
                    $("#footer").append(key + ": " + sw_status + "<br>");
                }

                switch (sw_status) {
                    case 0:
                        $("#" + key).prop('checked', false);
                        $("label[for='" + key + "']").removeClass('redButton');
                        break;
                    case 1:
                        $("#" + key).prop('checked', true);
                        $("label[for='" + key + "']").removeClass('redButton');
                        break;
                    case 2:
                        $("label[for='" + key + "']").addClass('redButton');
                        break;
                    case 3://general error
                        $("#footer").empty().html(key);
                        led("red")
                        break;
                    case 4://feedback ok
                        led("green")
                        break;
                    default:
                        $("#led").attr("src", imgQuery + "red");
                        $("#footer").empty().html("strange sw_status " + sw_status)
                        console.log("strange sw_status " + sw_status + " type: " + typeof sw_status)
                        break;
                }

            });

            buttonrefresh();
            oGroup = {}

        });

    }

    var timeout = null

    function led(color) {
        if (timeout)
            clearTimeout(timeout)

        $("#led").attr("src", imgQuery + color);
        if (color === "green") {
            timeout = setInterval(function () {
                $("#led").attr("src", imgQuery + "grey");
            }, 500);
        }

    }

    var test_status = 1
    var stop = false
    var timer = null
    function run_some_tests() {

        if (stop) {
            clearInterval(timer)
            stop = !stop
        } else {
            stop = !stop
            var aEl = []
            $('#format input[type=checkbox]').each(function () {
                aEl.push($(this).attr("id"));
            });
            var idx = 0;

            timer = setInterval(function () {

                var idswitch = aEl[idx++]
                if (idswitch === "test_button") {
                    idx = 0
                    test_status = 1 - test_status
                } else {
                    oGroup[idswitch] = test_status;
                    ajaxSwitch("switch", oGroup)
                }

            }, 500);
        }

    }

    ajaxSwitch("status", null)

});
