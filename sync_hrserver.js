/* HuReS - v0.1
 * Humble Remote Switch 
 * Simple http tool switching relays using node.js on Raspberry Pi.
 * https://github.com/pliski/HuReS
 * 
 * This was the first version of the server.
 * Totally synchronous and very basic. 
 * Not maintened actually, I'm more interester in the Async version.
 * 
 * Usage:
 * $ node sync_hrserver.js [port]
 * 
 */
var fs = require('fs');
var url = require('url');
var http = require('http');

var Gpio = require('onoff').Gpio,
        relay1 = new Gpio(21, 'out'),
        relay2 = new Gpio(23, 'out'),
        relay3 = new Gpio(24, 'out'),
        relay4 = new Gpio(25, 'out');

function exit() {
    relay1.unexport();
    relay2.unexport();
    relay3.unexport();
    relay4.unexport();
    process.exit();
}

var VERSION = '0.1';
console.log("Humble Relay Switch (synchronous) - vers. " + VERSION);
console.log();

var port = Number(process.argv[2]);

if (port === undefined || !port || port < 1) {
    port = 8888;
    console.log("Port not assigned. Assuming default: " + port)
}


var server = http.createServer(function (req, res) {

//    if (req.method != 'GET')
//        return res.end('send me a GET\n')

    oMyUrl = url.parse(req.url, true)
    myPath = oMyUrl.pathname
    console.dir(myPath)

    switch (myPath) {

        case '/test':
            myquery = oMyUrl.query
            console.dir(myquery)
            for (var k in myquery) {
                if (myquery.hasOwnProperty(k)) {
                    res.writeHead(200, {'Content-Type': 'text/html'})
                    res.end("Key is " + k + ", value is" + myquery[k]);
                }
            }
            break;

        case '/switch':

            myquery = oMyUrl.query
            console.dir(myquery)

            for (var key in myquery) {

                if (myquery.hasOwnProperty(key)) {

                    var signal = myquery[key]
                    msg = {};
                    feedBack = "ok";

                    switch (key) {

                        case 'switch1':
                            relay1.writeSync(Number(signal));
                            break;

                        case 'switch2':
                            relay2.writeSync(Number(signal));
                            break;

                        case 'switch3':
                            relay3.writeSync(Number(signal));
                            break;

                        case 'switch4':
                            relay4.writeSync(Number(signal));
                            break;

                        default:
                            feedBack = "Not Found";

                    }

                    console.log("feedBack:" + feedBack);

                    msg[key] = signal;
                    msg['feedback'] = feedBack;
                    myJson = JSON.stringify(msg);
                    res.writeHead(200, {'Content-Type': 'application/json'})
                    res.end(myJson)
                }
            }

            break;

        case '/index':
            src = fs.createReadStream('./index.html');
            src.pipe(res)
            break

        case '/mobile':
            console.log("mobile");
            src = fs.createReadStream('./mobile.html');
            src.pipe(res)
            break

        case '/mobiletest':
            console.log("mobiletest");
            src = fs.createReadStream('./mobiletest.html');
            src.pipe(res)
            break

        case '/js':
            console.log("js");
            src = fs.createReadStream('./hrs.js');
            src.pipe(res)
            break

        default:
            res.writeHead(404)
            res.end()
            break

    }

})

// MAIN
server.listen(port)
process.on('SIGINT', exit);
